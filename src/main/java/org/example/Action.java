package org.example;

@FunctionalInterface
public interface Action {
      double operation(double num1, double num2);
}
