package org.example;


import com.google.inject.Singleton;

@Singleton
public class Division implements Action {
    @Override
    public double operation(double num1, double num2) {
        return num1 / num2;
    }
}
