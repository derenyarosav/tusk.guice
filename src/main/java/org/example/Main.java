package org.example;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class Main {
    public static void main(String[] args) {

        Injector injector = Guice.createInjector(new MainModule());

        Action addition = injector.getInstance(Addition.class);
        Subtraction subtraction = injector.getInstance(Subtraction.class);
        Multiplication multiplication = injector.getInstance(Multiplication.class);
        Division division = injector.getInstance(Division.class);

        System.out.println(addition.operation(234, 57));
        System.out.println(subtraction.operation(345, 87));
        System.out.println(multiplication.operation(45,7));
        System.out.println(division.operation(100,20));

        
    }
}